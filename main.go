package main

import (
	"database/sql"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"time"

	"github.com/dchest/passwordreset"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	mailer "github.com/kataras/go-mailer"
	"golang.org/x/crypto/bcrypt"
)

var db *sql.DB
var err error
var state string
var data map[string]string
var secret = []byte("_v!f4mj&67m73ueeum)*2e8v)=135a_n$!)@j@5pt1ug_x)pdt")
var store = sessions.NewCookieStore([]byte("SF2aGlBoxrDi4ovP35cEscVWg2t2IW6i"))

// User details for registration
type User struct {
	ID       int
	Email    string
	Password string
}

// ProfileData - User profile details
type ProfileData struct {
	Email        string
	FullName     string
	ContactEmail string
	Address      string
	Phone        string
}

// Connect to Database
func connectDB() {
	uri := fmt.Sprintf("bb2174a44daa47:f3c1b0aa@(us-cdbr-iron-east-04.cleardb.net:3306)/heroku_5e1d092d0557dfb")

	if db, err = sql.Open("mysql", uri); err != nil {
		return
	}

	if err != nil {
		log.Fatalln(err)
	}

	err = db.Ping()
	if err != nil {
		log.Fatalln(err)
	}
}

func routes() {
	r := mux.NewRouter()

	r.HandleFunc("/", index)
	r.HandleFunc("/register", register)
	r.HandleFunc("/profile", profile)
	r.HandleFunc("/login", login)
	r.HandleFunc("/reset/{token}", checkForgotToken).Methods("GET")
	r.HandleFunc("/reset/{token}", updatePassword).Methods("POST")
	r.HandleFunc("/forgot", forgotPassword)
	r.HandleFunc("/logout", logout)
	r.PathPrefix("/css/").Handler(http.StripPrefix("/css/", http.FileServer(http.Dir("dist/css/"))))
	r.PathPrefix("/images/").Handler(http.StripPrefix("/images/", http.FileServer(http.Dir("dist/images/"))))
	r.PathPrefix("/dist/").Handler(http.StripPrefix("/dist/", http.FileServer(http.Dir("dist/"))))
	http.Handle("/", r)
	// log.Fatal(http.ListenAndServe(":"+os.Getenv("PORT"), r))

	srv := &http.Server{
		Handler: r,
		Addr:    "127.0.0.1:8080",
	}

	fmt.Println("Server running on port :8080")
	log.Fatal(srv.ListenAndServe())
}

func outputHTML(w http.ResponseWriter, filename string, data interface{}) {
	t, err := template.ParseFiles(filename)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	if err := t.Execute(w, data); err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
}

func checkErr(w http.ResponseWriter, r *http.Request, err error) bool {
	if err != nil {
		fmt.Println(r.Host + r.URL.Path)
		http.Redirect(w, r, r.Host+r.URL.Path, 301)
		return false
	}
	return true
}

// User in database
func queryUser(email string) User {
	var users = User{}
	err = db.QueryRow(`
		SELECT id, 
		email, 
		password 
		FROM users WHERE email=?
		`, email).
		Scan(
			&users.ID,
			&users.Email,
			&users.Password,
		)
	return users
}

func fetchData(userEmail string) {
	var p ProfileData

	// Get user profile details from database
	err = db.QueryRow("SELECT email, full_name, contact_email, address, phone FROM users where email = ?", userEmail).Scan(&p.Email, &p.FullName, &p.ContactEmail, &p.Address, &p.Phone)
	if err != nil {
		fmt.Println(err.Error())
	}

	// Data for HTML
	data = map[string]string{
		"email":        userEmail,
		"fullName":     p.FullName,
		"contactEmail": p.ContactEmail,
		"address":      p.Address,
		"phone":        p.Phone,
	}
}

func index(w http.ResponseWriter, r *http.Request) {
	// Start session if user is logged in
	session, _ := store.Get(r, "current-session")
	sessionEmail := session.Values["email"]

	// If user is not authenticated redirect to login page
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		http.Redirect(w, r, "/login", 302)
		return
	}

	fetchData(sessionEmail.(string))

	// Execute HTML template with data
	outputHTML(w, "templates/index.html", data)
}

func register(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.ServeFile(w, r, "templates/register.html")
		return
	}

	// Get values from the form
	email := r.FormValue("email")
	password := r.FormValue("password")
	passwordConfirm := r.FormValue("password_confirm")
	users := queryUser(email)

	if (User{}) == users {
		if len(password) != 0 && password == passwordConfirm {
			// Hash password
			hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)

			// If there is no error save user details to database
			if len(hashedPassword) != 0 && checkErr(w, r, err) {
				stmt, err := db.Prepare("INSERT INTO users SET email=?, password=?")
				if err == nil {
					session, _ := store.Get(r, "current-session")

					_, err := stmt.Exec(&email, &hashedPassword)

					// Start session after sign up
					session.Values["email"] = &email
					session.Values["authenticated"] = true
					session.Save(r, w)

					// Redirect to Edit profile page
					http.Redirect(w, r, "/profile", 302)

					if err != nil {
						http.Error(w, err.Error(), http.StatusInternalServerError)
						return
					}
				}
			}
		} else {
			http.Redirect(w, r, "/register", 301)
		}
	} else {
		message := "Error: Email is already registered!"
		data = map[string]string{
			"message": message,
		}

		outputHTML(w, "templates/register.html", data)
		return
	}
}

func profile(w http.ResponseWriter, r *http.Request) {
	// Get session value
	session, _ := store.Get(r, "current-session")
	sessionEmail := session.Values["email"]

	// Get values from the form
	fullName := r.FormValue("full_name")
	contactEmail := r.FormValue("contact_email")
	address := r.FormValue("address")
	phone := r.FormValue("phone")

	fetchData(sessionEmail.(string))

	// If user isn't authenticated redirect to login page
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		http.Redirect(w, r, "/", 302)
		return
	}

	if auth, ok := session.Values["authenticated"].(bool); ok || auth {
		if r.Method != "POST" {
			outputHTML(w, "templates/profile.html", data)
			return
		}
	}

	stmt, err := db.Prepare("UPDATE users SET full_name=?, contact_email=?, address=?, phone=? WHERE email=?")
	if err == nil {
		_, err := stmt.Exec(&fullName, &contactEmail, &address, &phone, &sessionEmail)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		// Redirect to index page
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}
}

func login(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.ServeFile(w, r, "templates/login.html")
		return
	}

	// Generate random token for session state
	session, _ := store.Get(r, "current-session")

	// Form values
	email := r.FormValue("email")
	password := r.FormValue("password")
	users := queryUser(email)

	// Check if hashed password matches to its plaintext equivalent
	var hashedPassword = bcrypt.CompareHashAndPassword([]byte(users.Password), []byte(password))
	var fetchedEmail string
	var fetchedPassword string

	// Error messages, check for wrong credentials
	err = db.QueryRow("SELECT email, password FROM users WHERE email = ?", &email).Scan(&fetchedEmail, &fetchedPassword)
	if err != nil {
		// Check if email is registered
		if fetchedEmail == "" {
			message := "Error: The email address you entered isn't registered!"
			data = map[string]string{
				"message": message,
			}

			outputHTML(w, "templates/login.html", data)
		}
	}
	if err == nil {
		// If email is registered, then check if the password is correct
		if bcrypt.CompareHashAndPassword([]byte(fetchedPassword), []byte(password)) != nil {
			message := "Error: The password you entered is incorrect!"
			data = map[string]string{
				"message": message,
			}

			outputHTML(w, "templates/login.html", data)
		}
	}

	// In case of success log in and store session values
	if hashedPassword == nil {
		// Login success
		session.Values["email"] = users.Email
		session.Values["authenticated"] = true
		session.Save(r, w)
		http.Redirect(w, r, "/", 302)
	} else {
		http.Redirect(w, r, "/login", 301)
	}
}

func logout(w http.ResponseWriter, r *http.Request) {
	// Revoke users authentication
	session, _ := store.Get(r, "current-session")
	session.Values["email"] = ""
	session.Values["authenticated"] = false
	session.Save(r, w)
	http.Redirect(w, r, "/", 302)
}

func routerHandlerFunc(router *mux.Router) http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		router.ServeHTTP(res, req)
	}
}

func main() {
	connectDB()
	routes()

	defer db.Close()
}

// PASSWORD RESET
func forgotPassword(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.ServeFile(w, r, "templates/forgot.html")
		return
	}

	recoveryEmail := r.FormValue("email")
	// var message string

	var retrievedHash string
	row := db.QueryRow("SELECT password FROM users WHERE email = ?", recoveryEmail)
	err := row.Scan(&retrievedHash)
	if err != nil {
		// Show error if the email isn't registered
		if err == sql.ErrNoRows {
			message := "Error: The email address you entered isn't registered!"
			data = map[string]string{
				"message": message,
			}

			outputHTML(w, "templates/forgot.html", data)
			return
		}
	}

	passwordValue, _ := getPasswordHash(recoveryEmail)
	// Create temporary token
	resetPasswordToken := passwordreset.NewToken(recoveryEmail, 1*time.Hour, passwordValue, secret)

	// Update password with temporary token
	stmt, err := db.Prepare("UPDATE users SET password=? WHERE email=?")
	if err == nil {
		stmt.Exec(resetPasswordToken, recoveryEmail)
		if err != nil {
			panic(err)
		}
	}

	http.Redirect(w, r, "/", 301)

	// Sender configuration
	config := mailer.Config{
		Host:     "smtp.gmail.com",
		Username: "bekatesting1996@gmail.com",
		Password: "bekapassword1996",
		FromAddr: "bekatesting1996@gmail.com",
		Port:     587,
		// Enable UseCommand to support sendmail unix command
		UseCommand: false,
	}

	// Initalize a new mail sender service
	sender := mailer.New(config)

	// Email subject
	subject := "Password reset"

	// Rich message body
	content := `<h2>Hello</h2> 
				<p>You recently requested to reset your password for your Agile Forest account. Please click the button below to reset it:</p>
				<br/>
				<p><a href="http://localhost:8080/reset/` + resetPasswordToken + `" style="background: #49abfe; padding: 1em 1.5em; color: white; text-decoration: none; border-radius: 4px; font-weight: bold;">Reset Password</a></p>
				<br/>
				<p style="color:red">If you didn't request to reset your password, please ignore this email.</p>`

	// Recepient
	to := []string{recoveryEmail}

	// Send email
	send := sender.Send(subject, content, to...)
	if send != nil {
		println("error while sending the e-mail: " + send.Error())
	}
}

func checkForgotToken(w http.ResponseWriter, r *http.Request) {
	// Get token value from the uri
	vars := mux.Vars(r)
	token := vars["token"]

	// Check if the token value matches with one from database
	_, err := passwordreset.VerifyToken(token, getPasswordHash, secret)
	if err != nil {
		// return
	}

	http.ServeFile(w, r, "templates/reset.html")
	return
}

func updatePassword(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	token := vars["token"]

	if r.Method != "POST" {
		outputHTML(w, "templates/reset.html", token)
		return
	}
	password := r.FormValue("password")
	passwordConfirm := r.FormValue("password_confirm")

	if len(password) != 0 && password == passwordConfirm {
		// Hash password
		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)

		// Update password in database
		if len(hashedPassword) != 0 && checkErr(w, r, err) {
			stmt, err := db.Prepare("UPDATE users SET password=? WHERE password=?")
			if err == nil {
				stmt.Exec(hashedPassword, token)
				if err != nil {
					panic(err)
				}

				// Redirect to home page
				http.Redirect(w, r, "/", 302)
			}
		}
	}
}

func getPasswordHash(email string) ([]byte, error) {
	var err error
	var retrievedHash string

	// Retrieve password from the database
	row := db.QueryRow("SELECT password FROM users WHERE email = ?", email)
	err = row.Scan(&retrievedHash)
	if err != nil {
		panic(err.Error())
	}

	password := []byte(retrievedHash)
	return password, nil
}
