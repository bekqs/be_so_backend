// Google autocompletion for addresses
var placeSearch, address;

function initAutocomplete() {
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    address = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */
        (document.getElementById('address')), {
            types: ['geocode']
        });

    // When the user selects an address from the dropdown, populate the address
    // fields in the form.
    address.addListener('place_changed', fillInAddress);
}

function fillInAddress() {
    // Get the place details from the autocomplete object.
    var place = address.getPlace();
    address.value = place.formatted_address;
}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            address.setBounds(circle.getBounds());
        });
    }
}

// DOM elements
const form = document.forms['form'];
const messageDiv = document.querySelector('.message');
const message = document.querySelector('.message__text');

// Event Listeners
if (document.body.contains(form)) {
    const password = form['password'];
    const passwordConfirm = form['password_confirm'];

    // Check password length
    function passwordLenght() {
        if (password.value.length >= 8) {
            document.getElementById('check-1').style.color = '#43f461';
            document.getElementById('cond-1').style.textDecoration = 'line-through';
        } else {
            document.getElementById('check-1').style.color = '#f44368';
            document.getElementById('cond-1').style.textDecoration = 'none';
        }
    }

    // Check if passwords match
    function passwordMatch() {
        if (password.value === passwordConfirm.value && password.value != "") {
            document.getElementById('check-2').style.color = '#43f461';
            document.getElementById('cond-2').style.textDecoration = 'line-through';
        } else {
            document.getElementById('check-2').style.color = '#f44368';
            document.getElementById('cond-2').style.textDecoration = 'none';
        }
    }

    // Error message for registration page
    function registerError(e) {
        if (password.value != passwordConfirm.value || password.value.length < 8) {
            e.preventDefault();

            message.innerHTML = `Error: Something went wrong! User registration failed. Please try again.`;
            errorToggle();
        }

        if (password.value != passwordConfirm.value && password.value.length >= 8) {
            message.innerHTML = `Error: Passwords don't match!`;
            password.value = '';
            passwordConfirm.value = '';
        } else if (password.value.length < 8 || passwordConfirm.value.length < 8) {
            message.innerHTML = `Error: Password must contain at least 8 characters!`;
            password.value = '';
            passwordConfirm.value = '';
        }
    }

    // Event listeners
    password.addEventListener('keyup', () => passwordLenght())
    password.addEventListener('focusout', () => passwordLenght())
    passwordConfirm.addEventListener('keyup', () => passwordMatch())
    form.addEventListener('submit', e => registerError(e))
}

// Toggle error messages
function errorToggle() {
    if (message.innerHTML != '{{ .message }}' && message.innerHTML.length > 0) {
        messageDiv.classList.add('active');
    } else {
        messageDiv.classList.remove('active');
    }
}

window.addEventListener('load', () => errorToggle())